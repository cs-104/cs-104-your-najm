#! /bin/bash
#  System status

aws iam list-users | awk -F user/ ' { print $2}' | awk '{ print $1} '
echo "instance:"

aws route53 list-hosted-zones --output table 
echo "hosted zones:"

aws organizations describe-organization --output
echo "organisation available:"

aws ec2 describe-subnets  --output table 
echo "subnet:"                                                                                               

aws s3 ls --profile default --output table
echo "s3 profile:"

aws ec2 describe-instances --output table
echo "ec2 instance:"

aws ec2 describe-vpcs  --output table 
echo "vpcs:"




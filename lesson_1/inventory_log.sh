CPU Memory AND Disk usage report from bastion server
# Author: shosan najeem
# Website: bastion-node.dev-minds.com
## format:  Hostname, DateTime, CPU%, Mem%, Disk%
HOSTNAME=$(hostname)
DATET=$(date "+%Y-%m-%d %H: %M: %S") 
CPUSAGE=$(top –b –n 1 –d1 | grep “Cpu(s)” |awk '{print $2}' |awk –F. '{print $1}')
MEMUSAGE=$(free | grep Mem |awk '{print $3/$2 * 100.0}')
DISKUSAGE=$( df -P | column -t |awk '{print $5}' | tail -n 0 | sed 's/%//g')
   echo  "'HostName, DateTime, CPU(%), Mem(%), Disk(%)'"
   echo -e "$HOSTNAME, $DATET, $CPUSAGE, $MEMUSAGE, $DISKUSAGE:
